/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.ifpb.dac;

import edu.ifpb.dac.model.Empregado;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

/**
 *
 * @author Luciana
 */

@Entity
public class Projeto implements Serializable{

    @Id
    private int id;
    private String descricao;
    @ManyToMany(mappedBy = "projetos",cascade = {CascadeType.ALL})
    private List<Empregado> empregado;
            
            
    public Projeto() {
    }

    public void addEmp (Empregado empregado){
        this.empregado.add(empregado);
    }
     public void removeEmp (Empregado empregado){
        this.empregado.remove(empregado);
    }
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<Empregado> getEmpregado() {
        return empregado;
    }

    public void setEmpregado(List<Empregado> empregado) {
        this.empregado = empregado;
    }

    @Override
    public String toString() {
        return "Projeto{" + "id=" + id + ", descricao=" + descricao + ", empregado=" + empregado + '}';
    }

        
    
    
    
}

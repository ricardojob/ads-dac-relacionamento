package edu.ifpb.dac;

import java.io.Serializable;
import javax.persistence.Id;
import javax.persistence.Entity;

@Entity
public class Dependente implements Serializable {
    
    @Id
    private int Id;
    private String nome;

    public Dependente() {
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    
}

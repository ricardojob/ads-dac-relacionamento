/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.ifpb.dac;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 *
 * @author Joao
 */
@Entity
public class Endereco {
    @Id
    @GeneratedValue
    private int id;
    private String rua;
    private String bairro;

    public Endereco(int id, String rua, String bairro) {
        this.id = id;
        this.rua = rua;
        this.bairro = bairro;
    }

    public Endereco() {
    }
    
    

    public int getId() {
        return id;
    }

    public String getRua() {
        return rua;
    }

    public String getBairro() {
        return bairro;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }
    
    
}

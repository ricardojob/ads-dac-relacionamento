package edu.ifpb.dac;

import edu.ifpb.dac.model.Empregado;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 *
 * @author Izaquiel Cruz
 */
@Entity
public class Faculdade implements Serializable{
    
    @Id
    private long id;
    private String descricao;
    
    @OneToMany(mappedBy="faculdade")
    private List<Empregado> empregados = new ArrayList<>();

    public Faculdade() {
    }
    
    
    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    public List<Empregado> getEmpregados() {
        return empregados;
    }

    public void setEmpregados(List<Empregado> empregados) {
        this.empregados = empregados;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

}

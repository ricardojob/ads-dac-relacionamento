/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ifpb.dac.model;


import edu.ifpb.dac.Dependente;
import edu.ifpb.dac.Dependente;
import edu.ifpb.dac.Faculdade;
import edu.ifpb.dac.Endereco;
import edu.ifpb.dac.Projeto;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import javax.persistence.OneToOne;

import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author Junior
 */
@Entity
public class Empregado implements Serializable{
    @Id
    @GeneratedValue
    private int id;
    
    private String nome;
    
    @OneToOne
    private Endereco endereco;
    @OneToMany
    private Collection<Dependente> dependentes;
    @ManyToMany
    private Collection<Projeto> projetos;
    
    @ManyToOne
    private Faculdade faculdade;
    
    
    public Empregado() {
    }

    public Faculdade getFaculdade() {
        return faculdade;
    }

    public void setFaculdade(Faculdade faculdade) {
        this.faculdade = faculdade;
    }

    public Empregado(Endereco endereco) {
        this.endereco = endereco;
    }

    public Empregado(String nome, Endereco endereco) {
        this.nome = nome;
        this.endereco = endereco;
    }

    public void addDep(Dependente dependente){
        this.dependentes.add(dependente);
    }
    
    public void removerDep(Dependente dependente){
        this.dependentes.remove(dependente);
    }
    
    public void addProj(Projeto projeto){
        this.projetos.add(projeto);
    }
    
    public void removerProj(Projeto projeto){
        this.projetos.remove(projeto);
    }
    
    
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    
}
